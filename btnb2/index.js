/**
 * BT1:
 * 
 * Đầu vào:
 * lương ngày làm
 * số ngày làm
 *
 * các bước thực hiện:
 * tạo 2 biến luongLam và ngayLam
 * tạo biến tienLuong
 * gán 2 giá trị cho 2 biến luongLam và ngayLam
 * sử dụng công thức
 * tienLuong = luongLam * ngayLam 
 * 
 * 
 * Đầu ra:
 * Tiền lương
 * 
 * 
 */
function tienLuong(){
    var luongNgay= document.getElementById("luongNgay").value;
    var ngayLamValue = document.getElementById("ngayLam").value;
    var tinhLuong = ngayLamValue * luongNgay;
    console.log('tinhLuong: ', tinhLuong);

    document.getElementById("tinhLuong").innerHTML =` ${tinhLuong} Đồng `;
}
/**
 * BT2:
 * 
 * Đầu vào:
 * 5 số thực
 *
 * các bước thực hiện:
 * tạo 5 biến 5 số thưc
 * tạo biến trungBinhCong
 * gán 5 giá trị cho 5 số thực
 * sử dụng công thức
 * trungBinhCong = (a + b + c + d + e)/5 
 * 
 * 
 * Đầu ra:
 * trungBinhCong
 * 
 * 
 */

function tinhTBC(){
    var soThu1 = document.getElementById("soThu1").value;
    var soThu2 = document.getElementById("soThu2").value;
    var soThu3 = document.getElementById("soThu3").value;
    var soThu4 = document.getElementById("soThu4").value;
    var soThu5 = document.getElementById("soThu5").value;
    var tinhTong =( parseInt(soThu1)+parseInt(soThu2) + parseInt(soThu3) + parseInt(soThu4) + parseInt(soThu5)) /5;
    console.log('tinhTBC: ', tinhTBC);
    
    document.getElementById("tinhTong").innerHTML = `${tinhTong}`;
}
/**
 * BT3:
 * 
 * Đầu vào:
 * Giá tiền chuyển đổi từ USD sang VND là 23.500VND
 * Số tiền usd cần chuyển đổi
 * 
 *
 * các bước thực hiện:
 * tạo 2 biến usdSangVnd và usd
 * gán giá trị 2 biến usdSangVnd và usd
 * tạo biến vnd
 * 
 * sử dụng công thức
 * vnd = usdSangVnd * usd
 * 
 * 
 * Đầu ra:
 * số tiền vnd đã chuyển đổi
 * 
 * 
 */
function quyDoiTien(){
    var usdSangVnd = 23500;
    var usd = document.getElementById("nhapUSD").value;

    var tinhTien = usdSangVnd * usd;
    document.getElementById("tinhTien").innerHTML = `${new Intl.NumberFormat().format(tinhTien)} VND`;
}
/**
 * BT4:
 * 
 * Đầu vào:
 * chiều dài
 * chiều rông
 * 
 *
 * các bước thực hiện:
 * tạo 2 biến chieuDai và chieuRong
 * gán giá trị 2 biến chieuDai và chieuRong
 * tạo 2 2 biến chuViHcn và dienTichHcn
 * 
 * sử dụng công thức
 * chuViHcn = (chieuDai + chieuRong) * 2
 * dienTichHcn = chieuDai * chieuRong
 * 
 * 
 * Đầu ra:
 * chuViHcn
 * dienTichHcn
 * 
 * 
 */
function tinhHCN(){
    var chieuDai = document.getElementById("chieuDai").value;
    var chieuRong = document.getElementById("chieuRong").value;
    var dienTich = chieuDai * chieuRong;
    console.log('dienTich: ', dienTich);
    var chuVi = (parseInt(chieuDai) + parseInt(chieuRong))*2;
    document.getElementById("xuatTinh").innerHTML =` Diện tích: ${dienTich} ; Chu Vi: ${chuVi} ` ;
}
/**
 * BT5:
 * 
 * Đầu vào:
 * nhập vào số có 2 chữ số
 * 
 * 
 *
 * các bước thực hiện:
 * tạo 3 biến n hangChuc hangDonvi 
 * 
 * sử dụng công thức
 * hangChuc = Math.floor(n/10) để lấy số chục
 * hangDonVi = Math.floor(n%10) để lấy số đơn vị
 * 
 * 
 * 
 * Đầu ra:
 * Tổng của 2 số hàng chục và hàng đơn vị cộng lại
 * 
 * 
 */
function tinhTong() {
    var n =document.getElementById("soThuc").value;
    var hangChuc = Math.floor(n/10);
    var hangDonVi = Math.floor(n%10);
    var sum = hangChuc + hangDonVi;  

    document.getElementById("sum").innerHTML = `${sum}`;
}