var dssv = [];

var localJson = localStorage.getItem("DSSV");
if(localJson){
    var dataDraw = JSON.parse(localJson);
    result = [];
    for(var index = 0 ; index < dataDraw.length; index++){
        var currentData = dataDraw[index];
        var sv = new sinhVien(currentData.ma, 
            currentData.ten,
            currentData.email,
            currentData.matKhau,
            currentData.diemLy,
            currentData.diemToan,
            currentData.diemHoa
            );
        result.push(sv);
    }
    dssv = result;
    renderDssv(dssv);
}

function themSv(){
    var newSv = layThongTinTuForm();
    dssv.push(newSv);

    luuDuLieuLocal();
    renderDssv(dssv);
    resetForm();
}

function luuDuLieuLocal(){
    var dssvJson = JSON.stringify(dssv);
    localStorage.setItem("DSSV", dssvJson);
}

function xoaSv(idSv){
   var index = dssv.findIndex(function(sv){
        return sv.ma == idSv;
   })
   if(index == -1){
    return;
   }
   dssv.splice(index,1);
   renderDssv(dssv);
//    luuDuLieuLocal();
}

function suaSv(idSv){
    var index = dssv.findIndex(function(sv){
        return sv.ma == idSv;
    })
    if(index == -1) return;
    
    var sv = dssv[index];
    console.log('sv: ', sv);
    showThongTinTuForm(sv);

    document.getElementById("txtMaSV").disabled = true;
 }

 function capNhatSv(){
    var svEdit = layThongTinTuForm();
    console.log('vEdit: ', svEdit);
    var index = dssv.findIndex(function(sv){
        return sv.ma == svEdit.ma;
    })
    if(index == -1) return;

    dssv[index] = svEdit;
    luuDuLieuLocal();
    renderDssv(dssv);
    resetForm();
 }

