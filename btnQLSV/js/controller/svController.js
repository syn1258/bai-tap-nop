function layThongTinTuForm(){
    var maSv = document.getElementById("txtMaSV").value.trim();
    var tenSv = document.getElementById("txtTenSV").value.trim();
    var email = document.getElementById("txtEmail").value.trim();
    var matKhau = document.getElementById("txtPass").value.trim();
    var diemLy = document.getElementById("txtDiemLy").value.trim();
    var diemToan = document.getElementById("txtDiemToan").value.trim();
    var diemHoa = document.getElementById("txtDiemHoa").value.trim();

    var sv = new sinhVien(maSv, tenSv, email, matKhau, diemLy, diemToan, diemHoa);
    return sv;
};

function renderDssv(list){
    var contentHTML ="";
    for(var i = 0 ; i < list.length; i++){
        var currentSv = list[i];
        var cunrrentTr =`<tr>
            <td>${currentSv.ma}</td>
            <td>${currentSv.ten}</td>
            <td>${currentSv.email}</td>
            <td>${currentSv.tinhDTB()}</td>
            
            <td>
            <button onclick="xoaSv('${currentSv.ma}')" class="btn btn-success">Xoá</button>
            <button onclick="suaSv('${currentSv.ma}')" class="btn btn-dark">Sửa</button>
            </td>

        </tr>`;
        contentHTML+= cunrrentTr;
    }
    document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function showThongTinTuForm(sv){
    document.getElementById("txtMaSV").value = sv.ma;
    document.getElementById("txtTenSV").value = sv.ten;
    document.getElementById("txtEmail").value = sv.email;
    document.getElementById("txtPass").value = sv.matKhau;
    document.getElementById("txtDiemLy").value = sv.diemLy;
    document.getElementById("txtDiemToan").value = sv.diemToan;
    document.getElementById("txtDiemHoa").value = sv.diemHoa;
    
}

function resetForm(){
    document.getElementById("formQLSV").reset();
}