var dsnv = [];
var dataLocal = localStorage.getItem("DSNV");
if(dataLocal){
    var dataDraw = JSON.parse(dataLocal);
    result = [];
    for(var index = 0 ; index < dataDraw.length; index++){
        var currentData = dataDraw[index];
        var nv = new nhanVien(
            currentData.tk, 
            currentData.hoTen,
            currentData.email,
            currentData.pass,
            currentData.ngayLam,
            currentData.luongCB,
            currentData.chucVu,
            currentData.gioLam
            );
        result.push(nv);
    }
    dsnv = result;
    renderDssv(dsnv);
}
function themNv(){
    var newNv = getInfoForm();
    dsnv.push(newNv);
    saveLocalStorage();
    renderDssv(dsnv);
}

function saveLocalStorage(){
    var dsnvJson = JSON.stringify(dsnv);
    localStorage.setItem("DSNV", dsnvJson);
}

function xoaNv(tknv){
    var index = dsnv.findIndex(function(nv){
       return nv.tk == tknv;
    })
    if (index == -1) return;
    dsnv.splice(index,1);
    renderDssv(dsnv);
}

function updateNv(tknv){
    var nvEdit = getInfoForm();
    console.log('nvEdit: ', nvEdit);
    var index = dsnv.findIndex(function(nv){
        return nv.tk == nvEdit.tk;
    })
    if(index == -1) return;
    dsnv[index] = nvEdit;
    renderDssv(dsnv);
}

function searchXepLoai() {
    var valueSearch = document.getElementById("searchName").value;
    var userSearch = dsnv.filter(function(value){
        return value.xepLoai().includes(valueSearch)
    })
    console.log('userSearch: ', userSearch);
    renderDssv(userSearch);
 }
