function nhanVien(_taiKhoan,_hoTen,_email,_passWord,_ngayLam,_luongCB,_chucVu,_gioLam){
    this.tk = _taiKhoan;
    this.hoTen = _hoTen;
    this.email = _email;
    this.pass = _passWord;
    this.ngayLam = _ngayLam;
    this.luongCB = _luongCB;
    this.chucVu = _chucVu;
    this.gioLam = _gioLam;
    
    this.tongLuong = function(){
        var tinhTong = 0;
        if(this.chucVu == "Sếp"){
            return tinhTong = this.luongCB *3;
        }else if(this.chucVu == "Trưởng phòng"){
            return tinhTong = this.luongCB *2;
        }else{
            return tinhTong = this.luongCB;
        }
    } 
    this.xepLoai = function(){
        var tinhXepLoai = "";
        if(this.gioLam >= 192){
            return tinhXepLoai = `nhân viên xuất sắc`;
        }else if(this.gioLam >= 176){
            return tinhXepLoai = `nhân viên giỏi`;
        }else if(this.gioLam >= 160){
            return tinhXepLoai = `nhân viên khá`;
        }else{
            return tinhXepLoai = `nhân viên trung bình`;
        }
    }
}
