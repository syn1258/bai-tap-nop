var numberArray = [];
var numberArrayPlus = [];
function themSo(){
    var sum = 0;
    var evenArray = 0;
    var aveCount = 0;
    var inputEl = document.querySelector("#txt-number");
    if(inputEl.value == ""){
        return;
    }
    var number = inputEl.value *1;
    
    numberArray.push(number);
    inputEl.value = "";
    var minArray = numberArray[0];
    for(var i =0; i < numberArray.length; i++){
        var number1 = numberArray[i];
        if(number1 > 0){
            sum += number1;
            aveCount++;
            numberArrayPlus.push(number1);
        }
        if(number1 % 2 == 0){
            evenArray = number1;
        }
        
        if(minArray > numberArray[i]){
            minArray = numberArray[i];
        } 
    }

    var minArrayPlus = numberArrayPlus[0];
    for(var j=0; j < numberArrayPlus.length; j++){
        if(numberArrayPlus != undefined){
            if(minArrayPlus > numberArrayPlus[j]){
                minArrayPlus = numberArrayPlus[j];
            }
        } 
        else{
            minArrayPlus = `Không có phần tử trong mảng`
        }   
    }  
    var contentHTML = `
    <p>Mảng đã nhập:${numberArray}</p>
    <p>BT1.tổng số dương: ${sum}</p>
    <p>BT2.đếm các số dương: ${aveCount}</p>
    <p>BT3.Số nhỏ nhất trong mảng: ${minArray}</p>
    <p>BT4.Số dương nhỏ nhất trong mảng: ${minArrayPlus}</p>
    <p>BT5.Số chẵn cuối cùng của mảng: ${evenArray}</p>
    `;

    document.getElementById("result").innerHTML = contentHTML;
}

function daoViTri(){
    var viTri1 = document.getElementById("txt-vi-tri-1").value;
    var viTri2 = document.getElementById("txt-vi-tri-2").value;
    
    var temp = numberArray[viTri1];
    numberArray[viTri1] = numberArray[viTri2];
    numberArray[viTri2] = temp;

    document.getElementById("result1").innerHTML = `Vị trí mảng sau khi đổi: ${numberArray}`;
}
function soSanh(){
    var negaCount = 0;
    var posiCount = 0;
    for(var i =0; i < numberArray.length; i++){
        var number2 = numberArray[i];
        if(number2 < 0){
            negaCount++;
        }else{
            posiCount++;
        }
        if(posiCount > negaCount){
            document.getElementById("result3").innerHTML = `Số dương: ${posiCount} > số âm: ${negaCount} `;
        }else{
            document.getElementById("result3").innerHTML = `Số dương: ${posiCount} < số âm: ${negaCount} `;
        }
    }
}
function sapXep(){
    // for(var i = 0; i < numberArray.length - 1; i++){
        for(var j = 0; i < numberArray.length - 1; j++){
            
            if(numberArray[j] > numberArray[j+1]){
                var temp = numberArray[j];
                numberArray[j] = numberArray[j+1];
                numberArray[j+1] = temp;
            }
        }
    // }
    document.getElementById("result2").innerHTML = `Sắp xếp tăng dần: ${numberArray} `;
}
